# Sign Up VCU

## Instructions

* Make a directory called `suv` and place an appropriate `signupvcu.json` file in there.
* Grab the `docker-compose.yml` from this repository.

Your working directory should look like this now.
``` 
.
|-- docker-compose.yml
`-- suv
    `-- signupvcu.json
```

* Run `docker-compose up` with the appropriate permissions in your working directory the night before all the people in your JSON file are able to sign up for classes.
* Tell tell the people in your JSON file to check their email the following day and validate the screenshot of their classes.

## JSON

To run this application, you need a properly formatted `signupvcu.json` file. Look at `EXAMPLE.json` for a reference. You will need a bot email's credentials and the plaintext passwords of everyone who you want to sign up for classes.